[https://github.com/BretFisher/udemy-docker-mastery]

## Installation
[download](store.docker.com) or `curl -sSL https://get.docker.com/ | sh`
[docker machine](https://docs.docker.com/machine/install-machine/)
[docker compose](https://docs.docker.com/compose/install/)


## Commands
Command format: `docker <command> <sub-command> (options)`
`docker version` 
`docker --help` 
`docker login` - login to your docker account to push images
`docker logout`
`docker info` - most config values of engine 
`docker run --name mongo -d mongo` - create a new mongo container and detach it (run in the background)
`docker start mongo` - start container
`docker stop mongo` - stop container
`docker container ls` - list all containers
`docker container top` - process list in one container
`docker container inspect` - details of one container config
`docker container stats` - performance stats for all containers
`docker container rm` - remove container
`docker container logs` - read logs on container
`docker container run -d mysql -e MYSQL_RANDOM_ROOT_PASSWORD=yes` - pass environment variable
`docker container -p 80:80` - expose host port
`docker container run -it` - start new container interactively
`docker container exec -it` - run additional command in existing container 
`docker container exec -it bash` - get a shell on container
`docker container run --publish 8080:80 --name webhost -d nginx:1.11 nginx` - Change the default config (`--publish 8080:80` is host:container ports, `--name webhost` specifies the container name, `nginx:1.11` specify version of image, final `nginx` changes command run on start)
`docker container inspect --format <config value> <container>` - Return config value for container 
`docker network ls` - Show networks 
`docker container run --network <network>` - Run container on specified network
`docker network inspect` - Inspect a network 
`docker network create --driver` - Create a network 
`docker network connect` - Attach a network to container 
`docker network disconnect` - Detach a network from container 
`docker image ls` - Show all installed images
`docker image tag <source image:tag> <target image:tag>` - Copy an existing image 
`docker image push <image>` - Push image to repository

## Dockerfile
[https://docs.docker.com/engine/reference/builder/]
